import json
import requests
import re
import warnings
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from statsmodels.tsa.seasonal import seasonal_decompose
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
from statsmodels.tsa.seasonal import seasonal_decompose
from pandas.plotting import autocorrelation_plot
from sklearn.metrics import mean_squared_error
from statsmodels.tsa.statespace.sarimax import SARIMAX

warnings.filterwarnings("ignore")

'''
api_base = 'https://www.data.qld.gov.au/api/3/action/resource_search?query=name:fuel'
api_key = 'accaa1fa-e4ef-40e9-97b7-517f8d9076f2'

headers = {
		'Accept': 'application/json',
		'Authorization': 'apikey {0}'.format(api_key)
            }

response = json.loads(requests.get(api_base, headers = headers).content)

urls = [item['url'] for item in response['result']['results']]
data_resources = [re.search('/resource/(.*?)/', item).group(1) for item in urls[1:-1]]

url_base = 'https://www.data.qld.gov.au/api/3/action/datastore_search?resource_id='

content = list()
tables = list()
i = 0

#different versions of the data cuts have different column names (with(out) undescores)
for resource in data_resources:
    
    urlcall = url_base + resource + '&limit=999999'
    content += [json.loads(requests.get(urlcall, headers = headers).content)]
    tables += [pd.DataFrame(data = [item.values() for item in content[i]['result']['records']],
                             columns = [item['id'].replace(' ', '_') for item in content[i]['result']['fields']]).drop('_id', axis = 1)]

    i += 1

df = pd.concat(tables)
    
result = df[df.Site_State == 'QLD'].sort_values('TransactionDateutc', ascending = False).drop(['Sites_Address_Line_1', 'Site_State'], axis = 1)
#result.reset_index().to_json('//Users//alexander//Desktop//Root//GovHack//fuel_prices_apiextract.json', orient = 'records', lines = True)
'''

result = pd.read_csv('//Users//alexander//Desktop//Root//GovHack//fuel_prices_apiextract.csv')

latest_prices = result.groupby(['Site_Name', 'Fuel_Type']).Price.last().reset_index() \
                    .merge(result[['Site_Name', 'Site_Latitude', 'Site_Longitude']].drop_duplicates(), on = 'Site_Name', how = 'left') \
                    .reset_index(drop = True) 
#                    .to_json('//Users//alexander//Desktop//Root//GovHack//latest_prices.json', orient = 'records', lines = True)#, header = True, index = False)

#df = pd.DataFrame(data = [item.values() for item in response['result']['records']], columns = [item['id'] for item in response['result']['fields']])

caltex = result[(result.Site_Name == 'Caltex Bracken Ridge')&(result.Fuel_Type == 'e10')][['TransactionDateutc', 'Price']]
caltex['date'] = pd.to_datetime(caltex.TransactionDateutc, infer_datetime_format = True)
caltex = caltex.drop('TransactionDateutc', axis = 1)
caltex = caltex.set_index('date')
caltex = caltex.sort_values('date')

caltex_wk = caltex.resample('W').mean()

decompose = seasonal_decompose(caltex_wk, model='multiplicative', freq=4)

def evaluate_arima_model(X, arima_order, arima_sorder):
        # prepare training dataset
        #train_size = int(len(X) * 0.75)
        #train, test = X[0:train_size], X[train_size:]
        test_size = 2
        train, test = X[:-test_size], X[-test_size:]
        history = [x for x in train]
        # make predictions
        predictions = list()

        model = SARIMAX(history, order = arima_order, seasonal_order = arima_sorder, enforce_stationarity = False, enforce_invertibility = False)
        model_fit = model.fit(disp=0)
        
        for t in range(len(test)):
                yhat = model_fit.forecast()[0]
                predictions.append(yhat)
                history.append(test[t])
        # calculate out of sample error
        error = mean_squared_error(test, predictions)
        return error

#print('ARIMA%s MSE=%.3f' % ([order, s_order],mse)) -> after 'if' with the same indent
#print('Best ARIMA%s MSE=%.3f' % (best_cfg, best_score)) -> before return
def evaluate_models(dataset, p_values, d_values, q_values, s_values):
        best_score, best_cfg = float('inf'), None
        for p in p_values:
                for d in d_values:
                        for q in q_values:
                                order = (p,d,q)
                                for p in p_values:
                                        for q in q_values:
                                                for s in s_values:
                                                        s_order = (p,0,q,s)
                                                        try:
                                                                mse = evaluate_arima_model(dataset, order, s_order)
                                                                if mse < best_score:
                                                                        best_score, best_cfg = mse, [order, s_order]
                                                        except:
                                                                continue
        print('Best ARIMA%s MSE=%.3f' % (best_cfg, best_score))
        return(best_cfg)

p = range(0,5)
d = range(0,5)
q = range(0,5)
s = range(0,6)

series = caltex_wk.Price.astype('float64').values
conf = evaluate_models(series, p, d, q, s)

test_size = 4
train, test = series[:-test_size], series[-test_size:]

model = SARIMAX(train, order = conf[0], s_order = conf[1], enforce_stationarity = False, enforce_invertibility = False)
model_fit = model.fit()

forecasts = list()
forecasts = np.append(train, model_fit.forecast(len(test)+1))


