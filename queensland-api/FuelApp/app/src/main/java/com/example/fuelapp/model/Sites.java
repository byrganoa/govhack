package com.example.fuelapp.model;

import java.util.ArrayList;
import java.util.List;

public class Sites {

    List<Site> mySites;

    public Sites() {
        this.mySites = new ArrayList<Site>();
    }

    public List<Site> getMySites() {
        return mySites;
    }

    public void setMySites(ArrayList<Site> mySites) {
        this.mySites = mySites;
    }

    public void addSite(Site newSite){
        mySites.add(newSite);
    }

    @Override
    public String toString() {
        return "Sites{" +
                "mySites=" + mySites +
                '}';
    }
}
