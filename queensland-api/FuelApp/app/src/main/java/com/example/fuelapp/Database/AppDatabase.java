package com.example.fuelapp.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.fuelapp.model.SiteContract;

/**
 *
 * Basic database class for the application
 *
 * The only class that should use this is AppProvider.
 *
 */

public class AppDatabase extends SQLiteOpenHelper {

    private static final String TAG = "AppDatabase";

    public static final String DATABASE_NAME = "FuelPrice.db";
    public static final int DATABASE_VERSION = 1;

    private static AppDatabase instance = null;

    private AppDatabase(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
        Log.d(TAG,"AppDatabase: constructor");
    }

    /**
     *
     * get an instance of the app's singleton database helpder object
     */

    public static AppDatabase getInstance(Context context){
        if(instance == null){
            Log.d(TAG,"getInstance: creating new instance");
            instance = new AppDatabase(context);
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.d(TAG,"onCreate: starts");
        String sSQL;
        sSQL = "CREATE TABLE " + SiteContract.TABLE_NAME + " ("
                + SiteContract.Columns._ID + " INTEGER PRIMARY KEY NOT NULL, "
                + SiteContract.Columns.SITENAME + " TEXT NOT NULL, "
                + SiteContract.Columns.SITEBRAND + " TEXT, "
                + SiteContract.Columns.SITEADDRESS + " TEXT NOT NULL, "
                + SiteContract.Columns.SUBURB + " TEXT NOT NULL, "
                + SiteContract.Columns.STATE + " TEXT NOT NULL, "
                + SiteContract.Columns.POSTCODE + " INTEGER NOT NULL, "
                + SiteContract.Columns.LONG + " INTEGER NOT NULL, "
                + SiteContract.Columns.LAT + " INTEGER NOT NULL);";
        Log.d(TAG,sSQL);
        sqLiteDatabase.execSQL(sSQL);
        Log.d(TAG,"onCreate: ends");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        Log.d(TAG,"onUpgrade: starts");
        switch(i){
            case 1:
                //upgrade logic from version 1
                break;
            default:
                throw new IllegalStateException("onUpgrade() with unknown newVersion: "+i1);
        }
        Log.d(TAG,"onUpgrade: ends");
    }
}
