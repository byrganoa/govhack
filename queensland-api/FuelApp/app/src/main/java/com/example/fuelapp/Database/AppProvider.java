package com.example.fuelapp.Database;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.fuelapp.model.SiteContract;

public class AppProvider extends ContentProvider {

    private static final String TAG = "AppProvider";

    private AppDatabase mOpenHelper;
    public static final UriMatcher sUriMatcher = buildUriMatcher();
    public static final String CONTENT_AUTHORITY = "com.example.fuelapp.provider";
    public static final Uri CONTENT_AUTHORITY_URI = Uri.parse("content://"+CONTENT_AUTHORITY);

    public static final int SITE = 100;
    public static final int SITE_ID = 101;

    public static final int FUELPRICE = 200;
    public static final int FUELPRICE_ID = 201;

    private static UriMatcher buildUriMatcher(){
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);

        matcher.addURI(CONTENT_AUTHORITY, SiteContract.TABLE_NAME,SITE);
        matcher.addURI(CONTENT_AUTHORITY,SiteContract.TABLE_NAME + "/#",SITE_ID);
//        matcher.addURI(CONTENT_AUTHORITY,FuelContract.TABLE_NAME,FUELPRICE);
//        matcher.addURI(CONTENT_AUTHORITY,FuelContract.TABLE_NAME,FUELPRICE_ID);
        return matcher;
    }


    @Override
    public boolean onCreate() {
        mOpenHelper = AppDatabase.getInstance(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] strings, @Nullable String s, @Nullable String[] strings1, @Nullable String s1) {
        Log.d(TAG,"query: called with URI "+uri);
        final int match = sUriMatcher.match(uri);
        Log.d(TAG,"query: match is "+match);
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        switch(match){
            case SITE:
                queryBuilder.setTables(SiteContract.TABLE_NAME);
                break;
            case SITE_ID:
                queryBuilder.setTables(SiteContract.TABLE_NAME);
                long siteId = SiteContract.getSiteId(uri);
                queryBuilder.appendWhere(SiteContract.Columns._ID +" = "+siteId);
                break;
//            case FUELPRICE:
//                queryBuilder.setTables(FuelPriceContract.TABLE_NAME);
//                break;
//            case FUELPRICE_ID:
//                queryBuilder.setTables(FuelPriceContract.TABLE_NAME);
//                long fuelpriceId = SiteContract.getFuelPriceId(uri);
//                queryBuilder.appendWhere(FuelPriceContract.Columns._ID +" = "+fuelpriceId);
//                break;
            default:
                throw new IllegalArgumentException("Unknown URI: "+uri);
        }
        SQLiteDatabase db = mOpenHelper.getReadableDatabase();
        return queryBuilder.query(db,strings,s,strings1,null,null,s1);
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        final int match = sUriMatcher.match(uri);
        switch(match){
            case SITE:
                return SiteContract.CONTENT_TYPE;
            case SITE_ID:
                return SiteContract.CONTENT_ITEM_TYPE;
            //            case FUELPRICE:
//                return FuelPriceContract.CONTENT_TYPE;
//                break;
//            case FUELPRICE_ID:
//                return FuelPriceContract.CONTENT_ITEM_TYPE;
//                break;
            default:
                throw new IllegalArgumentException("Unknown URI: "+uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        Log.d(TAG,"Entering insert, called with uri: "+uri);
        final int match = sUriMatcher.match(uri);
        Log.d(TAG,"match is "+match);
        final SQLiteDatabase db;
        Uri returnUri;
        long recordId;
        switch(match){
            case SITE:
                db = mOpenHelper.getWritableDatabase();
                recordId = db.insert(SiteContract.TABLE_NAME,null,contentValues);
                if(recordId >= 0){
                    returnUri = SiteContract.buildSiteUri(recordId);
                } else {
                    throw new android.database.SQLException("Failed to insert into "+uri.toString());
                }
                break;
//            case FUELPRICE:
//                db = mOpenHelper.getWritableDatabase();
//                recordId = db.insert(FuelPriceContrac.TABLE_NAME,null,contentValues);
//                if(recordId >= 0){
//                    returnUri = FuelPriceContract.buildFuelPriceUri(recordId);
//                } else {
//                    throw new android.database.SQLException("Failed to insert into "+uri.toString());
//                }
//                break;
            default:
                throw new IllegalArgumentException("Unknown uri: "+uri);
        }
        Log.d(TAG,"Exiting insert, returning "+returnUri);
        return returnUri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        Log.d(TAG,"delete called with uri "+uri);
        final int match = sUriMatcher.match(uri);
        Log.d(TAG,"match is "+match);
        final SQLiteDatabase db;
        int count;
        String selectionCriteria;
        switch(match){
            case SITE:
                db = mOpenHelper.getWritableDatabase();
                count = db.delete(SiteContract.TABLE_NAME,s,strings);
                break;
            case SITE_ID:
                db = mOpenHelper.getWritableDatabase();
                long siteId = SiteContract.getSiteId(uri);
                selectionCriteria = SiteContract.Columns._ID + " = "+siteId;
                if((s != null) && (s.length()>0)){
                    selectionCriteria += " AND (" + s + ")";
                }
                count = db.delete(SiteContract.TABLE_NAME,selectionCriteria,strings);
                break;
//            case FUELPRICE:
//                db = mOpenHelper.getWritableDatabase();
//                count = db.delete(FuelPriceContract.TABLE_NAME,s,strings);
//                break;
//            case FUELPRICE_ID:
//                db = mOpenHelper.getWritableDatabase();
//                long fuelpriceId = FuelPriceContract.getFuelPriceId(uri);
//                selectionCriteria = FuelPriceContract.Columns._ID + " = "+fuelpriceId;
//                if((s != null) && (s.length()>0)){
//                    selectionCriteria += " AND (" + s + ")";
//                }
//                count = db.delete(FuelPriceContract.TABLE_NAME,s,strings);
//                break;
            default:
                throw new IllegalArgumentException("Unknown uri: "+uri);
        }
        Log.d(TAG,"Exiting update, returning "+count);
        return count;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings) {
        Log.d(TAG,"update called with uri "+uri);
        final int match = sUriMatcher.match(uri);
        Log.d(TAG,"match is "+match);
        final SQLiteDatabase db;
        int count;
        String selectionCriteria;
        switch(match){
            case SITE:
                db = mOpenHelper.getWritableDatabase();
                count = db.update(SiteContract.TABLE_NAME,contentValues,s,strings);
                break;
            case SITE_ID:
                db = mOpenHelper.getWritableDatabase();
                long siteId = SiteContract.getSiteId(uri);
                selectionCriteria = SiteContract.Columns._ID + " = "+siteId;
                if((s != null) && (s.length()>0)){
                    selectionCriteria += " AND (" + s + ")";
                }
                count = db.update(SiteContract.TABLE_NAME,contentValues,selectionCriteria,strings);
                break;
//            case FUELPRICE:
//                db = mOpenHelper.getWritableDatabase();
//                count = db.update(FuelPriceContract.TABLE_NAME,contentValues,s,strings);
//                break;
//            case FUELPRICE_ID:
//                db = mOpenHelper.getWritableDatabase();
//                long fuelpriceId = FuelPriceContract.getFuelPriceId(uri);
//                selectionCriteria = FuelPriceContract.Columns._ID + " = "+fuelpriceId;
//                if((s != null) && (s.length()>0)){
//                    selectionCriteria += " AND (" + s + ")";
//                }
//                count = db.update(FuelPriceContract.TABLE_NAME,contentValues,s,strings);
//                break;
            default:
                throw new IllegalArgumentException("Unknown uri: "+uri);
        }
        Log.d(TAG,"Exiting update, returning "+count);
        return count;
    }
}
