package com.example.fuelapp.model;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

import static com.example.fuelapp.Database.AppProvider.CONTENT_AUTHORITY_URI;
import static com.example.fuelapp.Database.AppProvider.CONTENT_AUTHORITY;

public class SiteContract {

    public static final String TABLE_NAME = "Sites";

    public static class Columns{

        public static final String _ID = BaseColumns._ID;
        public static final String SITENAME = "SiteName";
        public static final String SITEBRAND = "SiteBrand";
        public static final String SITEADDRESS = "SiteAddress";
        public static final String SUBURB = "SiteSuburb";
        public static final String STATE = "SiteState";
        public static final String POSTCODE = "SitePostCode";
        public static final String LAT = "SiteLat";
        public static final String LONG = "SiteLong";

        private Columns(){
            // private constructor to prevent instantiation
        }
    }

    /**
     * The URI to access the Sites table
     */
    public static final Uri CONTENT_URI = Uri.withAppendedPath(CONTENT_AUTHORITY_URI,TABLE_NAME);

    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd." + CONTENT_AUTHORITY + "." +TABLE_NAME;
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd."+CONTENT_AUTHORITY + "."+TABLE_NAME;

    public static Uri buildSiteUri(long siteId){
        return ContentUris.withAppendedId(CONTENT_URI,siteId);
    }

    public static long getSiteId(Uri uri){
        return ContentUris.parseId(uri);
    }
}
