package com.example.fuelapp.ui.main;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.example.fuelapp.DAO.SiteContractDao;
import com.example.fuelapp.model.Site;
import com.example.fuelapp.model.Sites;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class GetJsonData extends AsyncTask<String, Void, Sites> implements GetRawData.OnDownloadComplete {

    private static final String TAG = "GetJsonData";

    private Sites newSites = null;
    private String mBaseURL;

    private final OnDataAvailable mCallBack;
    private Context context;
    private boolean runningOnSameThread = false;

    public interface OnDataAvailable {
        void onDataAvailable(Sites data, DownloadStatus status);
    }

    public GetJsonData(OnDataAvailable callBack, String baseURL, Context context) {
        Log.d(TAG, "GetJsonData called");
        mBaseURL = baseURL;
        this.context = context;
        mCallBack = callBack;
    }

    void executeOnSameThread(String searchCriteria) {
        Log.d(TAG, "executeOnSameThread starts");
        runningOnSameThread = true;
        String destinationUri = createUri(searchCriteria);

        GetRawData getRawData = new GetRawData(this);
        getRawData.execute(destinationUri);
        Log.d(TAG, "executeOnSameThread ends");
    }

    @Override
    protected void onPostExecute(Sites newSites) {
        Log.d(TAG, "onPostExecute starts");

        if(mCallBack != null) {
            mCallBack.onDataAvailable(newSites, DownloadStatus.OK);
        }
        Log.d(TAG, "onPostExecute ends");
    }

    @Override
    protected Sites doInBackground(String... params) {
        Log.d(TAG, "doInBackground starts");
        Log.d(TAG,params[0]);
        String destinationUri = createUri(params[0]);

        GetRawData getRawData = new GetRawData(this);
        getRawData.runInSameThread(destinationUri);
        Log.d(TAG, "doInBackground ends");
        return newSites;
    }

    private String createUri(String searchCriteria) {
        Log.d(TAG, "createUri starts");

        return Uri.parse(mBaseURL).buildUpon()
                .build().toString();
    }

    @Override
    public void onDownloadComplete(String data, DownloadStatus status) {
        Log.d(TAG, "onDownloadComplete starts. Status = " + status);

        if(status == DownloadStatus.OK) {
            newSites = new Sites();
            try {
                JSONObject jsonData = new JSONObject(data);
                JSONObject itemsResult = jsonData.getJSONObject("result");
                JSONArray recordsResult = itemsResult.getJSONArray("records");
                Log.d(TAG,recordsResult.toString());


                for(int i=0; i<recordsResult.length(); i++) {
                    JSONObject newSite = recordsResult.getJSONObject(i);
                    String siteId = newSite.getString("_id");
                    String siteName = newSite.getString("Site_Name");
                    String siteBrand = newSite.getString("Site_Brand");
                    String siteAddress = newSite.getString("Sites_Address_Line_1");
                    String siteSuburb = newSite.getString("Site_Suburb");
                    String siteState = newSite.getString("Site_State");
                    String sitePostCode = newSite.getString("Site_Post_Code");
                    String siteLat = newSite.getString("Site_Latitude");
                    String siteLong = newSite.getString("Site_Longitude");
                    String fuelType = newSite.getString("Fuel_Type");
                    String fuelPrice = newSite.getString("Price");
                    String transDate = newSite.getString("TransactionDateutc");
                    Site mySite = new Site();
                    mySite.setId(Integer.parseInt(siteId));
                    mySite.setSiteName(siteName);
                    mySite.setSiteAddress(siteAddress);
                    mySite.setSiteBrand(siteBrand);
                    mySite.setSuburb(siteSuburb);
                    mySite.setState(siteState);
                    mySite.setPostCode(Integer.parseInt(sitePostCode));
                    mySite.setSiteLat(Double.parseDouble(siteLat));
                    mySite.setSiteLong(Double.parseDouble(siteLong));

                    SiteContractDao siteContractDao = new SiteContractDao(context);
                    siteContractDao.insertSite(mySite.getSiteName(),
                            mySite.getSiteBrand(),
                            mySite.getSiteAddress(),
                            mySite.getSuburb(),
                            mySite.getPostCode(),
                            mySite.getState(),
                            mySite.getSiteLat(),
                            mySite.getSiteLong());

                    newSites.addSite(mySite);

                    Log.d(TAG, "onDownloadComplete");
                }
            } catch(JSONException jsone) {
                jsone.printStackTrace();
                Log.e(TAG, "onDownloadComplete: Error processing Json data " + jsone.getMessage());
                status = DownloadStatus.FAILED_OR_EMPTY;
            }
        }

        if(runningOnSameThread && mCallBack != null) {
            // now inform the caller that processing is done - possibly returning null if there
            // was an error
            mCallBack.onDataAvailable(newSites, status);
        }

        Log.d(TAG, "onDownloadComplete ends");
    }

}
