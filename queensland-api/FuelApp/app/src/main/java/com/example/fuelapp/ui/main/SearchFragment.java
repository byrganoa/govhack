package com.example.fuelapp.ui.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.fuelapp.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;

public class SearchFragment extends Fragment implements OnMapReadyCallback{

    private GoogleMap mMap;
    private MapView mMapView;
    View mView;
    Polyline currentPolyline;

    public SearchFragment(){

    }

    public static SearchFragment newInstance() {
        return new SearchFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.search_fragment,container,false);
        return mView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Spinner mySpinner = (Spinner) view.findViewById(R.id.fuel_filter);
        ArrayAdapter<String> myAdapter = new ArrayAdapter<String>(getContext(),android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.fuel));
        myAdapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
        mySpinner.setAdapter(myAdapter);

        Spinner mySpinner2 = (Spinner) view.findViewById(R.id.distance_filter);
        ArrayAdapter<String> myAdapter2 = new ArrayAdapter<String>(getContext(),android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.distance));
        myAdapter2.setDropDownViewResource(android.R.layout.simple_list_item_1);
        mySpinner2.setAdapter(myAdapter2);


        mMapView = (MapView) mView.findViewById(R.id.map);
        if(mMapView != null){
            mMapView.onCreate(null);
            mMapView.onResume();
            mMapView.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(getContext());
        mMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        MarkerOptions place1, place2, place3,place4;
        place1 = new MarkerOptions().position(new LatLng(-27.4069059,153.0451114)).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)).title("You are here");
        place2 = new MarkerOptions().position(new LatLng(-27.408151,153.047515)).title("Freedom Fuels Wooloowin").snippet("Diesel\t145.9");
        place3 = new MarkerOptions().position(new LatLng(-27.359821, 153.016417)).title("Coles Express Aspley").snippet("Diesel\t145.9");
        place4 = new MarkerOptions().position(new LatLng(-27.472378, 153.059422)).title("Pacific Petroleum Galloways Hill").snippet("Diesel\t144.9");
        googleMap.addMarker(place1);
        googleMap.addCircle(new CircleOptions().center(new LatLng(-27.4069059,153.0451114)).strokeColor(0x220000FF).radius(7500).fillColor(0x220000FF));
        googleMap.addMarker(place2);
        googleMap.addMarker(place3);
        googleMap.addMarker(place4);
        String url = getUrl(place1.getPosition(),place2.getPosition(),"driving");
    }

    // Function to draw the line
    private String getUrl(LatLng ori, LatLng dest,String directionMode){
        // Origin route
        String str_origin = "origin="+ori.latitude+","+ori.longitude;
        // Destination route
        String str_destination = "destination="+dest.latitude+","+dest.longitude;
        // Mode
        String mode = "mode="+directionMode;
        //Building the parameters to the web service
        String parameters = str_origin+"&"+str_destination+"&"+mode;
        //Output format
        String output = "json";
        // Building the url to the web service
        String url = "https://maps.google.apis.com/maps/api/directions/"+output+"?"+parameters+"&key="+getString(R.string.google_maps_key);
        return url;

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
