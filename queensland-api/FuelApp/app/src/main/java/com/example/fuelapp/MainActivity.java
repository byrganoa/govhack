package com.example.fuelapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.Manifest;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toolbar;


import com.example.fuelapp.DAO.SiteContractDao;
import com.example.fuelapp.model.Sites;
import com.example.fuelapp.ui.main.DownloadStatus;
import com.example.fuelapp.ui.main.GetJsonData;
import com.example.fuelapp.ui.main.HomeFragment;
import com.example.fuelapp.ui.main.LocateFragment;
import com.example.fuelapp.ui.main.MoreFragment;
import com.example.fuelapp.ui.main.SearchFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;


public class MainActivity extends AppCompatActivity implements GetJsonData.OnDataAvailable {

    private static final String TAG = "MainActivity";
    /* * * * * * * * * * *
     * Defined Variables *
     * * * * * * * * * * */
    private Fragment selectedFragment;
    private Fragment homeFragment;
    private Fragment searchFragment;
    private Fragment locateFragment;
    private Fragment moreFragment;

    private Toolbar toolbar;
    private BottomNavigationView navigation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        getSupportActionBar().hide();
        ActivityCompat.requestPermissions(MainActivity.this,
                new String[]{Manifest.permission.INTERNET},
                1);
        // set up tool bar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setVisibility(View.GONE);

        // set up
        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        homeFragment = new HomeFragment();
        searchFragment = new SearchFragment();
        locateFragment = new LocateFragment();
        moreFragment = new MoreFragment();
        selectedFragment = homeFragment;

        // make the navigation page when first to using app
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container,selectedFragment).commit();

        int id = getIntent().getIntExtra("id",0);
        String sid = getIntent().getStringExtra("sid");
        int key = getIntent().getIntExtra("key",0);
        if(id == 1){
            navigation.setSelectedItemId(R.id.navigation_home);
        }
        if(id == 2){
            navigation.setSelectedItemId(R.id.navigation_search);
        }
        if(id == 3){
            navigation.setSelectedItemId(R.id.navigation_locate);
        }
        if(id == 4){
            navigation.setSelectedItemId(R.id.navigation_more);
        }

//        GetJsonData getJsonData = new GetJsonData(this,"https://www.data.qld.gov.au/api/3/action/datastore_search?resource_id=59846e12-4b39-40b7-81d5-7f24e9c9afbc&limit=5",this);
//        getJsonData.execute("something");
        Sites sites = new SiteContractDao(this).querySite(null,null);
    }

    /* * * * * *
     * Methods *
     * * * * * */

    public BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener()
    {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item)
        {
            switch (item.getItemId())
            {
                case R.id.navigation_home:
                    selectedFragment = homeFragment;
                    break;
                case R.id.navigation_search:
                    selectedFragment = searchFragment;
                    break;
                case R.id.navigation_locate:
                    selectedFragment = locateFragment;
                    break;
                case R.id.navigation_more:
                    selectedFragment = moreFragment;
                    break;
                default:
                    break;
            }

            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, selectedFragment).addToBackStack( null ).commit();

            return true;
        }
    };

    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();
        Log.d("back",String.valueOf(count));
        Fragment fragment = getSupportFragmentManager().getPrimaryNavigationFragment();

        toolbar.setVisibility(View.GONE);
        navigation.setVisibility(View.VISIBLE);
        navigation.setSelectedItemId(R.id.navigation_home);
    }

    @Override
    public void onDataAvailable(Sites data, DownloadStatus status) {
        if(status == DownloadStatus.OK) {
            Log.d(TAG, "onDataAvailable: data is " + data);

        } else {
            // download or processing failed
            Log.e(TAG, "onDataAvailable failed with status " + status);
        }
    }
}
