package com.example.fuelapp.DAO;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.example.fuelapp.model.Site;
import com.example.fuelapp.model.SiteContract;
import com.example.fuelapp.model.Sites;

public class SiteContractDao {

    private Context myContext;
    private static final String TAG = "SiteContractDao";
    private ContentResolver contentResolver;
    private static String[] projection = {SiteContract.Columns.SITENAME,
                SiteContract.Columns.SITEBRAND,
                SiteContract.Columns.SITEADDRESS,
                SiteContract.Columns.SUBURB,
                SiteContract.Columns.POSTCODE,
                SiteContract.Columns.STATE,
                SiteContract.Columns.LAT,
                SiteContract.Columns.LONG};

    public SiteContractDao(Context myContext) {
        this.myContext = myContext;
        this.contentResolver = myContext.getContentResolver();
    }

    public Uri insertSite(String siteName, String siteBrand, String siteAddress, String suburb, int postCode, String state, double lat, double longi){

        ContentValues values = new ContentValues();
        values.put(SiteContract.Columns.SITENAME,siteName);
        values.put(SiteContract.Columns.SITEBRAND,siteBrand);
        values.put(SiteContract.Columns.SITEADDRESS,siteAddress);
        values.put(SiteContract.Columns.SUBURB,suburb);
        values.put(SiteContract.Columns.POSTCODE,postCode);
        values.put(SiteContract.Columns.STATE,state);
        values.put(SiteContract.Columns.LAT,lat);
        values.put(SiteContract.Columns.LONG,longi);
        return contentResolver.insert(SiteContract.CONTENT_URI,values);
    }

    public Sites querySite(String selection, String[] selectionArgs){
        Cursor cursor = contentResolver.query(SiteContract.CONTENT_URI,
                projection,
                selection,
                selectionArgs,
                SiteContract.Columns.SITENAME);
        Sites returnSites = new Sites();
        if(cursor != null){
            Log.d(TAG,"onCreate: number of rows: "+cursor.getCount());
            while(cursor.moveToNext()){
                Site newSite = new Site();
                for(int i = 0;i< cursor.getColumnCount();i++){
                    Log.d(TAG,"onCreate: "+cursor.getColumnName(i)+": "+cursor.getString(i));
                    if(cursor.getColumnName(i) == SiteContract.Columns._ID && cursor.getString(i) != null){
                        newSite.setId(Integer.parseInt(cursor.getString(i)));
                    } else if (cursor.getColumnName(i) == SiteContract.Columns.SITENAME && cursor.getString(i) != null){
                        newSite.setSiteName(cursor.getString(i));
                    } else if (cursor.getColumnName(i) == SiteContract.Columns.SITEBRAND && cursor.getString(i) != null){
                        newSite.setSiteBrand(cursor.getString(i));
                    } else if (cursor.getColumnName(i) == SiteContract.Columns.SITEADDRESS && cursor.getString(i) != null){
                        newSite.setSiteAddress(cursor.getString(i));
                    } else if (cursor.getColumnName(i) == SiteContract.Columns.SUBURB && cursor.getString(i) != null){
                        newSite.setSuburb(cursor.getString(i));
                    } else if (cursor.getColumnName(i) == SiteContract.Columns.POSTCODE && cursor.getString(i) != null){
                        newSite.setPostCode(Integer.parseInt(cursor.getString(i)));
                    } else if (cursor.getColumnName(i) == SiteContract.Columns.STATE && cursor.getString(i) != null){
                        newSite.setState(cursor.getString(i));
                    } else if (cursor.getColumnName(i) == SiteContract.Columns.LAT && cursor.getString(i) != null){
                        newSite.setSiteLat(Double.parseDouble(cursor.getString(i)));
                    } else if (cursor.getColumnName(i) == SiteContract.Columns.LONG && cursor.getString(i) != null){
                        newSite.setSiteLong(Double.parseDouble(cursor.getString(i)));
                    }
                }
                Log.d(TAG,"onCreate: =====================");
            }
            cursor.close();
        } else{
            Log.d(TAG,"cursor is null");
        }
        return returnSites;
    }

}
