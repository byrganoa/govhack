package com.example.fuelapp.model;

public class Site {

    private int id;
    private String siteName;
    private String siteBrand;
    private String siteAddress;
    private String suburb;
    private String state;
    private int postCode;
    private double siteLat;
    private double siteLong;

    public Site() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getSiteBrand() {
        return siteBrand;
    }

    public void setSiteBrand(String siteBrand) {
        this.siteBrand = siteBrand;
    }

    public String getSiteAddress() {
        return siteAddress;
    }

    public void setSiteAddress(String siteAddress) {
        this.siteAddress = siteAddress;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getPostCode() {
        return postCode;
    }

    public void setPostCode(int postCode) {
        this.postCode = postCode;
    }

    public double getSiteLat() {
        return siteLat;
    }

    public void setSiteLat(double siteLat) {
        this.siteLat = siteLat;
    }

    public double getSiteLong() {
        return siteLong;
    }

    public void setSiteLong(double siteLong) {
        this.siteLong = siteLong;
    }

    @Override
    public String toString() {
        return "Site{" +
                "id=" + id +
                ", siteName='" + siteName + '\'' +
                ", siteBrand='" + siteBrand + '\'' +
                ", siteAddress='" + siteAddress + '\'' +
                ", suburb='" + suburb + '\'' +
                ", state='" + state + '\'' +
                ", postCode=" + postCode +
                ", siteLat=" + siteLat +
                ", siteLong=" + siteLong +
                '}';
    }
}
