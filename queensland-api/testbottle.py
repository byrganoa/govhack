from bottle import route, run, template, request, post, get
import numpy as np 
import pandas as pd 
import json
import pandasql as ps

df = pd.read_csv('fuel_prices_apiextract.csv', header=0)

@get('/getData')
def getData():
    #global df
    #a = {'name':'Sarah', 'age': 24, 'isEmployed': True }
    siteId = "\"" + str(request.query.siteId) + "\""
    fuelType = "\"" + str(request.query.fuelType) + "\""
    
    query = '''SELECT * FROM df where SiteId = ''' + siteId + '''and Fuel_Type = ''' + fuelType + ''' LIMIT 5 '''
    data = ps.sqldf(query, globals()).to_json(orient='records')
    
    #data = json.dumps(a[attribute])
    #data = json.dumps(a)
    return data

run(host='localhost', port=8080)